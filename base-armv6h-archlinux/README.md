Run the script on a ArchLinux machine to generate a new Archlinux Base image.
Should work on both x86 as ARM, but only tested on armv6h (Raspberry Pi 1B).

To my understanding it only takes the minimal set of libraries to be able to run pacman, so it shouldn't matter which packages are already installed on the host system.

Also, Docker containers don't have a kernel, only userland libraries. It uses the kernel from the host. Does this mean Docker is sort of like a chroot environment on steroids (e.g. better isolation features)?
