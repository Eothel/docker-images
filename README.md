This repository contains a number of docker images for armv6h (can be used on a Raspberry Pi 1B).

Based on _Arch Linux ARM_ (e.g. pacman)

Starting containers
===================

Use following command as a template

```
docker run --publish 80:8000 --restart always --name myContainer --detach myImage
```

This means:

- `--publish 80:8000` port forward host port 80 to container exposed 8000
- `--restart always` causes container to be restarted when host is rebooted
- `--name` gives a name to the container for easy reference later
- `--detach` runs detached from current console (e.g. gives you back the prompt after starting the container)
